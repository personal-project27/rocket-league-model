package com.rocket.league.model.user;

import com.rocket.league.model.rank.Rank;
import com.rocket.league.model.stats.StatsList;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.com.bot.model.user.SuperUser;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

import static com.rocket.league.model.Constant.DOCUMENT_USER;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Document(DOCUMENT_USER)
public class User extends SuperUser {

    @Builder.Default
    private List<Rank> rankList = new ArrayList<>();

    private StatsList statList;

    private String platForm;

    private String platUser;
}
