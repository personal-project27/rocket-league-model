package com.rocket.league.model.rank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Rank {
    private String rank;
    private String division;
    private String winStreak;
    private int level;

    private LocalDateTime lastUpdate;

}
