package com.rocket.league.model.stats;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StatsList {
	private Long id;
	private Long chatId;
	private LocalDateTime lastUpdate;

	private List<SingleStats> stats;

	public String printStats() {
		return stats.stream().map(SingleStats::printStats).collect(Collectors.joining()) + "\n";
	}

	public String printStatsWithoutUpdate() {
		return stats.stream().map(SingleStats::printStats).collect(Collectors.joining());
	}

}
