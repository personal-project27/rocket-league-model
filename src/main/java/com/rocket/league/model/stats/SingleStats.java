package com.rocket.league.model.stats;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.DecimalFormat;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SingleStats {
    private String displayValue;
    private String displayName;
    private String percentile;
    private String rank;

    public String printStats() {
        final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.#");

        final String closure = "</code>";
        final String start = "<code class='language-python'>";

        StringBuilder str = new StringBuilder();
        str
                .append("\n")
                .append("<b>")
                .append("\u25AA")
                .append(displayName.trim())
                .append("</b>")
                .append(": ")
                .append(displayValue);

        if (rank == null && percentile != null)
            str
                    .append("\n\u25ABMMR: ")
                    .append("<b>")
                    .append(displayName.equalsIgnoreCase("Goal Shot Ratio")
                            ? DECIMAL_FORMAT.format(Float.parseFloat(percentile))
                            : DECIMAL_FORMAT.format(100 - Float.parseFloat(percentile)))
                    .append("%")
                    .append("</b>");
        else if (rank != null && percentile != null)
            str
                    .append("\n\u25ABMMR: ")
                    .append(start)
                    .append(String.format("%,d", Integer.parseInt(rank)))
                    .append(closure)
                    .append(" (")
                    .append("<b>")
                    .append(displayName.equalsIgnoreCase("Goal Shot Ratio")
                            ? DECIMAL_FORMAT.format(Float.parseFloat(percentile))
                            : DECIMAL_FORMAT.format(100 - Float.parseFloat(percentile)))
                    .append("%")
                    .append("</b>")
                    .append(")\n");
        return str.toString();
    }
}
