package com.rocket.league.model.scraping;

import lombok.Data;

@Data
public class Segments {

    private Metadata metadata;
    private Stats stats;
}
