package com.rocket.league.model.scraping;

import lombok.Data;

@Data
public class SpecificStats {

    private String rank;
    private String percentile;
    private String displayName;
    private String displayValue;
}
