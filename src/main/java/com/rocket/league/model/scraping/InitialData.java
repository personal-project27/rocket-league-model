package com.rocket.league.model.scraping;

import lombok.Data;
import java.util.List;

@Data
public class InitialData {
    
    private List<Segments> segments;

}
