package com.rocket.league.model.scraping;

import lombok.Data;

@Data
public class Stats {

    private SpecificStats wins;
    private SpecificStats goals;
    private SpecificStats mVPs;
    private SpecificStats saves;
    private SpecificStats assists;
    private SpecificStats shots;
    private SpecificStats goalShotRatio;
    private SpecificStats score;
    private Tier tier;
    private Division division;
    private MatchesPlayed matchesPlayed;
    private WinStreak winStreak;

    public SpecificStats getmVPs() {
        return mVPs;
    }

    public void setmVPs(SpecificStats mVPs) {
        this.mVPs = mVPs;
    }
}

