package com.rocket.league.model.scraping;

import lombok.Data;

@Data
public class Division {

    private Metadata metadata;

}
