package com.rocket.league.model.scraping;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InitialDataSteam {
    
    private String platformUserIdentifier;
}
