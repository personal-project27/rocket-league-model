package com.rocket.league.model.scraping;

import lombok.Data;

@Data
public class WinStreak {

    private String displayValue;
}
