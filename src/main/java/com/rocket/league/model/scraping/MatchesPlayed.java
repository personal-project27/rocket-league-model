package com.rocket.league.model.scraping;

import lombok.Data;

@Data
public class MatchesPlayed {

    private String displayValue;
}
