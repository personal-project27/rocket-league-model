package com.rocket.league.model;

public class Constant {
    public static final String DOCUMENT_USER = "user";
    public static final String DOCUMENT_MENU = "menu";
    public static final String DOCUMENT_STATE = "state";
    public static final String DOCUMENT_MESSAGES = "messages";
    public static final String DOCUMENT_CUSTOM_BUTTON = "custom_button";
}
